﻿using System;
using System.Globalization;

namespace WhoIsWhoAPP.Models
{
    public class HistoryAttempt
    {
        public string DateSort => Date.ToString("MMMM", CultureInfo.CreateSpecificCulture("en-US"));

        public DateTime Date { get; set; }
        public int Points { get; set; }
    }
}
