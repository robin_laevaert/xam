﻿namespace WhoIsWhoAPP.Models
{
    public static class Pages
    {
        public const string NavigationPage = "NavigationPage";
        public const string HistoryPage = "HistoryPage";

        public static string Nav(string page)
        {
            return $"/{NavigationPage}/{page}";
        }

        public static string Root(string page)
        {
            return $"/{page}";
        }
    }
}
