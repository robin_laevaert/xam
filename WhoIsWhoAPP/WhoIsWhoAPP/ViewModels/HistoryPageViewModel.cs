﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Humanizer;
using Prism.Mvvm;
using Prism.Services;
using WhoIsWhoAPP.Helpers;
using WhoIsWhoAPP.Models;

namespace WhoIsWhoAPP.ViewModels
{
    public class HistoryPageViewModel : BindableBase
    {
        private readonly IPageDialogService pageDialogService;

        private string title;

        private string place;

        private string points;

        private string name;

        private string imageSource;

        public HistoryPageViewModel(IPageDialogService pageDialogService)
        {
            Title = "History";

            //Set test-data
            Place = 1.Ordinalize();
            Points = "5000";
            Name = "Mickey Mouse";
            ImageSource = "Mickey.jpg";
            var Attempts = new List<HistoryAttempt>();
            for (var i = 0; i < 5; i++)
            {
                Attempts.Add(new HistoryAttempt {Date = new DateTime(2019, 1, 1), Points = 1000});
                Attempts.Add(new HistoryAttempt {Date = new DateTime(2019, 2, 1), Points = 2000});
                Attempts.Add(new HistoryAttempt {Date = new DateTime(2019, 3, 1), Points = 3000});
                Attempts.Add(new HistoryAttempt {Date = new DateTime(2019, 4, 1), Points = 4000});
            }

            var sorted = from HistoryAttemptV2 in Attempts
                orderby HistoryAttemptV2.Date
                group HistoryAttemptV2 by HistoryAttemptV2.DateSort
                into attemptGroup
                select new Grouping<string, HistoryAttempt>(attemptGroup.Key, attemptGroup);
            AttemptsGrouped = new ObservableCollection<Grouping<string, HistoryAttempt>>(sorted);

            this.pageDialogService = pageDialogService;
        }

        public ObservableCollection<Grouping<string, HistoryAttempt>> AttemptsGrouped { get; set; }


        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }

        public string Place
        {
            get => place;
            set => SetProperty(ref place, value);
        }

        public string Points
        {
            get => points;
            set => SetProperty(ref points, value);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
        public string ImageSource
        {
            get => imageSource;
            set => SetProperty(ref imageSource, value);
        }
    }
}
