﻿using Microsoft.Identity.Client;
using PCLAppConfig;
using PCLAppConfig.FileSystemStream;
using Prism;
using Prism.Ioc;
using WhoIsWhoAPP.Models;

using WhoIsWhoAPP.ViewModels;
using WhoIsWhoAPP.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace WhoIsWhoAPP
{
    public partial class App
    {


        public static string Username = string.Empty;
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */

        public App() : this(null)
        {
        }

        public App(IPlatformInitializer initializer) : base(initializer)
        {
        }

        public static UIParent UiParent { get; set; }

        protected override async void OnInitialized()
        {
            ConfigurationManager.Initialise(PortableStream.Current);
            InitializeComponent();
            await NavigationService.NavigateAsync(Pages.HistoryPage);
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<HistoryPage, HistoryPageViewModel>();
        }
    }
}
